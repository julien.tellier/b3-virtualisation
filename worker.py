#!/usr/bin/python
import time, sys, pika, datetime
from pymongo import MongoClient
from bson.objectid import ObjectId
from subprocess import call, Popen, PIPE

NEWVM_PS_SCRIPT = "/powershell/create_vm.ps1"
EDITVM_PS_SCRIPT = "/root/test.ps1"
DELETEVM_PS_SCRIPT = "/root/test.ps1"

def newvm_callback(ch, method, properties, body):
    print " [*] New VM queued with ID %s" % body
    vm = vmsCollection.find_one({"_id": ObjectId(body)})
    print " [*] Creating VM %s" % vm['_id']
    print "       - Memory %s MB" % vm['memoire']
    print "       - vCPUs %s" % vm['cpu']
    logfile = open('/var/log/newvm-%s.log' % body, 'w')
    p = Popen(["powershell",
        "-File", NEWVM_PS_SCRIPT,
        "-VMID", body,
        "-MemoryGB", vm['memoire'],
        "-NumCpu", vm['cpu']
        ],
    cwd='/powershell', stdout=logfile, stderr=logfile)
    p.wait()
    if p.returncode == 0:
        print " [*] VM %s successfully created !" % vm['_id']
        vm['status'] = "Started at %s" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
        vmsCollection.save(vm)
    else:
        print " [X] VM %s NOT created ! View log file %s" % (vm['_id'], logfile.name)
        vm['status'] = "Failed to create, please contact support"
        vmsCollection.save(vm)

def deletevm_callback(ch, method, properties, body):
    print " [*] Remove VM : %s" % body

def editvm_callback(ch, method, properties, body):
    print " [*] Edit VM : %s" % body

def main():
    if len(sys.argv) > 1 and sys.argv[1] == "loop":
        while True:
            print "Hello !"
            time.sleep(5)
    else:
        mongoClient = MongoClient('mongodb://db:27017/')
        mongoDb = mongoClient.unikorn
        global vmsCollection
        vmsCollection = mongoDb.vms
        connection = pika.BlockingConnection(pika.ConnectionParameters('rabbit'))
        channel = connection.channel()
        channel.queue_declare(queue='new-vm')
        channel.queue_declare(queue='delete-vm')
        channel.queue_declare(queue='edit-vm')
        channel.basic_consume(newvm_callback, queue='new-vm', no_ack=True)
        channel.basic_consume(deletevm_callback, queue='delete-vm', no_ack=True)
        channel.basic_consume(editvm_callback, queue='edit-vm', no_ack=True)
        print " [*] Waiting for messages. To exit press CTRL+C)"
        channel.start_consuming()

if __name__ == '__main__':
    time.sleep(10)
    print " [*] Waiting stack fully started..."
    main()