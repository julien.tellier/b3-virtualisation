<?php
include('header.html.php');
require_once __DIR__.'/vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$m = new MongoDB\Client("mongodb://db");

$db = $m->unikorn;
$collection = $db->vms;

$variable = $collection->InsertOne($_POST);

$rabbitConn = new AMQPStreamConnection('rabbit', 5672, 'guest', 'guest');
$channel = $rabbitConn->channel();

$queueName = "new-vm";
$exchangeName = "amq.direct";
$bindingKey = "new";

$channel->queue_bind($queueName,$exchangeName,$bindingKey);

$msg = new AMQPMessage($variable->getInsertedId());
$channel->basic_publish($msg, $exchangeName, $bindingKey);

echo '<p>[x] VM '. $_POST['nomVm'] .' créée</p>';
echo '<p>Dans 15 minutes, vous pourrez vous connecter avec l\'adresse IP indiquée dans le champ ip en SSH</p>';
echo '<p>nom d\'utilisateur : '. $_POST['vmuser'] .'</p>';
echo '<p>mot de passe : '. $_POST['vmpassword'] .'</p>';
?>


<?php
echo '<a class="btn btn-default" href="index.php" role="button">Accueil</a>';
include('footer.html.php');
?>