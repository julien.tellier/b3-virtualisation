<?php
include('header.html.php');
require_once __DIR__.'/vendor/autoload.php';

$id = $_GET['id'];

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$rabbitConn = new AMQPStreamConnection('rabbit', 5672, 'guest', 'guest');
$channel = $rabbitConn->channel();

$queueName = "delete-vm";
$exchangeName = "amq.direct";
$bindingKey = "delete";

$channel->queue_bind($queueName,$exchangeName,$bindingKey);

$msg = new AMQPMessage($id);
$channel->basic_publish($msg, $exchangeName, $bindingKey);

echo "<p>[x] VM ". $id. " supprimée</p>";
echo '<p><a class="btn btn-default" href="index.php" role="button">Accueil</a></p>';
include('footer.html.php');
?>