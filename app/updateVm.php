<?php
include('header.html.php');
require_once __DIR__.'/vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$id = $_POST['id'];
$nomVm = $_POST['nomVm'];
$cpu = $_POST['cpu'];
$memoire = $_POST['memoire'];
$date = new DateTime();
$status = "Updated at ".$date->format('Y-m-d H:i:s');

$m = new MongoDB\Client("mongodb://db");

$db = $m->unikorn;
$collection = $db->vms;

$variable = $collection->updateOne(['_id'=>new MongoDB\BSON\ObjectID($id)],array('$set' => array("nomVm" => $nomVm,"cpu" => $cpu, "memoire" => $memoire, "status" => $status)));

$rabbitConn = new AMQPStreamConnection('rabbit', 5672, 'guest', 'guest');
$channel = $rabbitConn->channel();

$queueName = "edit-vm";
$exchangeName = "amq.direct";
$bindingKey = "edit";

$channel->queue_bind($queueName,$exchangeName,$bindingKey);

$msg = new AMQPMessage($id);
$channel->basic_publish($msg, $exchangeName, $bindingKey);

echo "<p> [x] VM ". $id ." mise à jour</p>";
echo "<p> Vous pourrez vous connecter avec vos identifiants dans 15 minutes</p>";
echo '<a class="btn btn-default" href="index.php" role="button">Accueil</a>';
include('footer.html.php');
?>