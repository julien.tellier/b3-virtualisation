<?php
include('header.html.php');
require_once __DIR__.'/vendor/autoload.php';

$m = new MongoDB\Client("mongodb://db");

$db = $m->unikorn;
$collection = $db->vms;
$id = $_GET['id'];
$vm = $collection->FindOne(['_id'=>new MongoDB\BSON\ObjectID($id)], ['limit'=>1]);

?>

<form action="updateVm.php" method="post">
  <div class="form-group">
    <label for="nomVm">Nom de la VM</label>
    <?php echo '<input type="text" class="form-control" id="nomVm" value="'.$vm->nomVm.'" name="nomVm">'; ?>
  </div>
  <div class="form-group">
    <label for="cpu">Nombre de processeurs</label>
    <select class="form-control" id="cpu" name="cpu">
    <?php
      echo ($vm->cpu == 1) ? '<option value="1" selected>1</option>' : '<option value="1">1</option>';
      echo ($vm->cpu == 2) ? '<option value="2" selected>2</option>' : '<option value="2">2</option>';
      echo ($vm->cpu == 4) ? '<option value="4" selected>4</option>' : '<option value="4">4</option>';
    ?>
    </select>
  </div>
  <div class="form-group">
    <label for="memoire">Mémoire vive</label>
    <select class="form-control" id="memoire" name="memoire">
    <?php
      echo ($vm->memoire == 2048) ? '<option value="2048" selected>2</option>' : '<option value="2048">2</option>';
      echo ($vm->memoire == 4096) ? '<option value="4096" selected>4</option>' : '<option value="4096">4</option>';
      echo ($vm->memoire == 8192) ? '<option value="8192" selected>8</option>' : '<option value="8192">8</option>';
    ?>
    </select>
  </div>
  <?php
  echo '<input type="text" id="id" name="id" style="display:none;" value="'.$id.'">';
  ?>
 <button type="submit" class="btn btn-default">Mettre à jour</button>

</form>

<?php include('footer.html.php'); ?>