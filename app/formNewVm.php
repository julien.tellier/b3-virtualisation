<?php
include('header.html.php');
require_once __DIR__.'/vendor/autoload.php';
?>

<form action="newVm.php" method="post">
  <div class="form-group">
    <label for="nomVm">Nom de la VM</label>
    <input type="text" class="form-control" id="nomVm" placeholder="Nom" name="nomVm">
  </div>
  <div class="form-group">
    <label for="cpu">Nombre de processeurs</label>
    <select class="form-control" id="cpu" name="cpu">
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="4">4</option>
    </select>
  </div>
  <div class="form-group">
    <label for="memoire">Mémoire vive</label>
    <select class="form-control" id="memoire" name="memoire">
      <option value="2048">2</option>
      <option value="4096">4</option>
      <option value="8192">8</option>
    </select>
  </div>
  <div class="form-group">
    <label for="vmuser">Nom d'utilisateur</label>
    <input type="text" class="form-control" id="vmuser" placeholder="Nom d'utilisateur" name="vmuser">
  </div>
  <div class="form-group">
    <label for="vmpassword">Mot de passe</label>
    <input type="text" class="form-control" id="vmpassword" placeholder="Mot de passe" name="vmpassword">
  </div>
  <?php 
  $time = new DateTime();
  echo '<input type="text" id="status" name="status" style="display:none;" value="Creating ... '.$time->format('Y-m-d H:i:s').'">';
  ?>
 <button type="submit" class="btn btn-default">Commander</button>

</form>

<?php include('footer.html.php'); ?>