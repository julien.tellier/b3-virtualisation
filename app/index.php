<?php
include('header.html.php');
require_once __DIR__.'/vendor/autoload.php';

$m = new MongoDB\Client("mongodb://db");

$db = $m->unikorn;
$collection = $db->vms;

$vmList = $collection->find();
?>
<table class="table">
  <tr>
    <th>ID</th>
    <th>Statut</th>
    <th>Utilisateur</th>
    <th>Mot de passe</th>
    <th>IP</th>
    <th>Nom de la VM</th>
    <th>Nombre de CPU</th>
    <th>RAM</th>
    <th>Opérations</th>
  </tr>

<?php
foreach($vmList as $vm){
  echo "<tr>";
  echo "<th>".$vm->_id."</th>";
  echo "<th>".$vm->status."</th>";
  echo (isset($vm->vmuser)) ? '<th>'. $vm->vmuser .'</th>' : '<th>-</th>';
  echo (isset($vm->vmpassword)) ? '<th>'. $vm->vmpassword .'</th>' : '<th>-</th>';
  echo (isset($vm->ip)) ? '<th>'. $vm->ip .'</th>' : '<th>-</th>';
  echo "<th>".$vm->nomVm."</th>";
  echo "<th>".$vm->cpu."</th>";
  echo "<th>".$vm->memoire." Mo</th>";
  echo '<th><a href="deleteVm.php?id='.$vm->_id.'"><i class="fa fa-trash" aria-hidden="true"></i></a><a href="formUpdateVm.php?id='.$vm->_id.'"><i class="fa fa-wrench" aria-hidden="true"></i></a></th>';
  echo "</tr>";
}
?>
</table>
<a class="btn btn-success" href="formNewVm.php" role="button">Nouvelle VM</a>
<?php include('footer.html.php'); ?>