param(
    [parameter(Mandatory=$True)]
        [string]$VMID,
    [parameter(Mandatory=$True)]    
        [string]$MemoryGB,
    [parameter(Mandatory=$True)]
        [string]$NumCpu
)

$VMName = "VM-$($VMID)"
Set-PowerCLIConfiguration -InvalidCertificateAction Ignore -Confirm:$false
#Connect to ESXi Server
Connect-VIServer -Server 192.168.24.10 -User root -Password password
#Stop the VM to allow changes
Stop-VM -VM $VMName -Confirm:$false
#Modify the VM setings
Set-VM -VM $VMName -NumCpu $NumCpu -MemoryGB $MemoryGB -Confirm:$false
#Restart the VM
Start-VM -VM $VMName -Confirm:$false
#disconnect from ESXi Server
#Disconnect-VIServer -Server 192.168.24.10