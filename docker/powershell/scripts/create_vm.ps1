﻿param(
    [parameter(Mandatory=$True)]
        [string]$VMID,
    [parameter(Mandatory=$True)]    
        [string]$MemoryGB,
    [parameter(Mandatory=$True)]
        [string]$NumCpu
    [parameter(Mandatory=$True)]
         [string]$IPAdress
    [parameter(Mandatory=$True)]
        [string]$VMUser
    [parameter(Mandatory=$True)]
        [string]$VMPassword
        
)

$VMName = "VM-$($VMID)"
$SRCHardDisk = '[datastore1] debian9/debian9_0.vmdk'
$DSTHardDisk = "[datastore1] $($VMName)/$($VMName).vmdk"

Set-PowerCLIConfiguration -InvalidCertificateAction Ignore -Confirm:$false
Connect-VIServer -Server 192.168.24.10 -User root -Password password
Get-HardDisk -VM debian9 | Copy-HardDisk -DestinationPath $DSTHardDisk
New-VM -Name $VMName -DiskPath $DSTHardDisk -MemoryGB $MemoryGB -NumCpu $NumCpu -GuestId debian9_64Guest
Invoke-VMScript -VM $VMName -ScriptText "ip address add $($IPAdress)/24 dev ens192 ; ip link set dev ens192 up; useradd $($VMUser) --create-home; echo $($VMUser):$($VMPassword) | chpassword" -GuestUser root GuestPassword root 
Start-VM $VMName
