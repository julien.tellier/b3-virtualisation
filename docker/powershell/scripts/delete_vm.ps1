param(
    [parameter(Mandatory=$True)]
        [string]$VMID
)

$VMName = "VM-$($VMID)"
Set-PowerCLIConfiguration -InvalidCertificateAction Ignore -Confirm:$false
Connect-VIServer -Server 192.168.24.10 -User root -Password password
Stop-VM -VM $VMName -Confirm:$false
Remove-VM $VMName -DeletePermanently -Confirm:$false
